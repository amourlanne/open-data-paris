import {Controller, Get, ResponseView} from "@tsed/common";

@Controller("/")
export class HomeController {

  @Get("/")
  @ResponseView("index")
  private async index() {
    return {};
  }
}

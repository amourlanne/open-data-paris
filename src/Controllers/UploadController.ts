import {Controller, Get, ResponseView} from "@tsed/common";

@Controller("/upload")
export class UploadController {

  @Get("/")
  @ResponseView("upload/index")
  private async index() {
    return {};
  }
}

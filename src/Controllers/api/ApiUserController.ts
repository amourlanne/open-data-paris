import {Controller, Get, ResponseView} from "@tsed/common";
import {UserService} from "../../Services/UserService";
import {User} from "../../Entities/User";

@Controller("/api/users")
export class ApiUserController {

  constructor(private readonly userService: UserService) {}

  @Get("/")
  private async getAll(): Promise<User[]> {
    return this.userService.getAll();
  }
}

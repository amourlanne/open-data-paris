import {ServerLoader} from "@tsed/common";


import Server from "./Server";

async function bootstrap() {
  try {
    const server = await ServerLoader.bootstrap(Server);

    await server.listen();
  } catch (error) {
    console.error(error);
  }
}

bootstrap();

import {PlatformTest} from "@tsed/common";
import superTest from "supertest";

import {createConnection} from "typeorm";

import typeormConnexionConfig from "../../config/packages/typeorm";
import Server from "../../src/Server";

let request;
let connexion;

beforeAll(async done => {
  connexion = await createConnection(typeormConnexionConfig)
  done();
});

beforeAll(PlatformTest.bootstrap(Server));
beforeAll(() => {
  request = superTest(PlatformTest.callback());
});

afterAll(PlatformTest.reset);
afterAll(async done => {
  await connexion.close()
  done()
});

describe("Interface tests", () => {
  it("GET '/' respond 200 and html", done => {
    request
        .get("/")
        .expect("Content-Type", /text\/html/)
        .expect(200, done);
  });
});

describe("API tests", () => {
  it("GET '/api/users' respond 200 and json", done => {
    request
        .get("/api/users")
        .expect("Content-Type", /json/)
        .expect(200, done);
  });
});


